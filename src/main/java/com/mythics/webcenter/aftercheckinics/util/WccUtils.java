package com.mythics.webcenter.aftercheckinics.util;

import intradoc.common.ExecutionContext;
import intradoc.common.ExecutionContextAdaptor;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.*;
import intradoc.filestore.FileStoreProvider;
import intradoc.filestore.FileStoreProviderLoader;
import intradoc.filestore.IdcFileDescriptor;
import intradoc.provider.Provider;
import intradoc.provider.Providers;
import intradoc.server.DirectoryLocator;
import intradoc.server.Service;
import intradoc.server.ServiceManager;
import intradoc.server.UserStorage;
import intradoc.shared.SharedObjects;
import intradoc.shared.UserData;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jonathan Hult
 */
public class WccUtils {

    public static void trace(final String message) {
        Report.trace(AfterCheckinICSStringConstants.COMPONENT_NAME, message, null);
    }

    public static void warn(final String message) {
        warn(message, null);
    }

    public static void warn(final String message, final Exception exception) {
        Report.warning(AfterCheckinICSStringConstants.COMPONENT_NAME, message, exception);
    }

    // Constants
    private static FileStoreProvider fsp;

    private WccUtils() {
        // do not allow instantiation
    }

    public static void setFileStoreProvider(final ExecutionContext ctx) {
        trace("Enter setFileStoreProvider");

        try {
            fsp = DirectoryLocator.m_fileStore;

            if (fsp == null) {
                if (ctx instanceof Service) {
                    fsp = ((Service) ctx).m_fileStore;
                    trace("Setting FileStoreProvider from Service");
                }
            }
            if (fsp == null) {
                try {
                    fsp = FileStoreProviderLoader.initFileStore(ctx);

                    if (fsp != null) {
                        trace("Set FileStoreProvider from initFileStore");
                    } else {
                        warn("Could not find FileStoreProvider");
                    }
                } catch (final ServiceException e) {
                    warn("Error retrieving FileStoreProvider", e);
                } catch (final DataException e) {
                    warn("Error retrieving FileStoreProvider", e);
                }
            }
        } finally {
            trace("Exit setFileStoreProvider");
        }
    }

    public static File getVaultFile(final DataBinder binder, final ExecutionContext ctx) throws ServiceException {
        return getFile(FileStoreProvider.R_PRIMARY, binder, ctx);
    }

    private static File getWebFile(final DataBinder binder) throws ServiceException {
        return getFile(FileStoreProvider.R_WEB, binder, null);
    }

    public static File getFile(final String rendition, final DataBinder binder, final ExecutionContext ctx) throws ServiceException {
        trace("Enter getFile with rendition: " + rendition);

        String filePath = "";

        ExecutionContext ctxForFile = ctx;
        if (ctxForFile == null) {
            ctxForFile = new ExecutionContextAdaptor();
        }
        try {
            binder.putLocal(FileStoreProvider.SP_RENDITION_ID, rendition);
            binder.putLocal(FileStoreProvider.SP_RENDITION_PATH, rendition);
            final IdcFileDescriptor descriptor = fsp.createDescriptor(binder, null, ctxForFile);
            trace("IdcFileDescriptor: " + descriptor);
            fsp.forceToFilesystemPath(descriptor, null, ctxForFile);
            filePath = fsp.getFilesystemPath(descriptor, ctxForFile);
            trace("getFile returned this file path: " + filePath);
            return new File(filePath);
        } catch (final Exception e) {
            warn("Exception retrieving file", e);
            throw new ServiceException(e);
        } finally {
            trace("Exit getFile with filePath: " + filePath);
        }
    }

    /**
     * This will never return null. If the item is not checked-out, it will return an empty string.
     *
     * @param dID
     * @return
     * @throws DataException
     */
    public static String getCheckedOutBy(final String dID) throws DataException {
        trace("Enter getCheckedOutBy with dID: " + dID);

        String checkedOutBy = "";

        try {
            final DataBinder docInfo = getDocInfoDataBinder(dID);

            // I'm not sure if this could return null or not be let's ensure it does not
            checkedOutBy = docInfo.getLocal(AfterCheckinICSStringConstants.KEY_CHECKOUT_USER);

            if (checkedOutBy == null) {
                checkedOutBy = "";
            }

            return checkedOutBy;
        } finally {
            trace("Exit getCheckedOutBy with checkedOutBy: " + checkedOutBy);
        }
    }

    /**
     * Retrieves the latest revision (as an Integer) for the specified dDocName.
     * If there is an issue (such as content item has no released revision), then it will return -1.
     *
     * @param dDocName the content dDocName to build the DataBinder object for.
     * @return {@link DataBinder} the built DataBinder object.
     */
    public static int getLatestRevision(final String dDocName) {
        trace("Enter getLatestRevision with dDocName: " + dDocName);

        // Service that retrieves information only for the latest revision of a released content item.
        // If the content item has no released revision, the service returns a StatusCode of -1, with a corresponding StatusMessage value.
        // It only returns the DOC_INFO result set (no revision, workflow, or subscription information).
        // Because it does not have a template it returns only raw data.
        // https://docs.oracle.com/middleware/1221/wcc/services-reference/GUID-5443AA3F-DDE9-4E91-BB41-93651ABE7FFF.htm#GUID-AA809B63-A875-41D0-84E7-AF7E837041A2
        final DataBinder serviceBinder = getNewDataBinder();
        serviceBinder.putLocal(AfterCheckinICSStringConstants.IDC_SERVICE, AfterCheckinICSStringConstants.DOC_INFO_LATESTRELEASE);
        serviceBinder.putLocal(AfterCheckinICSStringConstants.KEY_DOC_NAME, dDocName);

        final int defaultRetVal = -1;

        try {
            executeService(serviceBinder);

            final ResultSet docInfoSet = serviceBinder.getResultSet(AfterCheckinICSStringConstants.DOC_INFO_RS);
            if (docInfoSet != null && !docInfoSet.isEmpty()) {
                setFirstRow(docInfoSet, serviceBinder);
                docInfoSet.closeInternals();

                return DataBinderUtils.getInteger(serviceBinder, AfterCheckinICSStringConstants.KEY_DOC_ID, defaultRetVal);
            } else {
                return defaultRetVal;
            }
        } catch (final ServiceException e) {
            warn("Failed to retrieve Content Info for dDocName: " + dDocName, e);
            return defaultRetVal;
        } catch (final DataException e) {
            warn("Failed to retrieve Content Info for dDocName: " + dDocName, e);
            return defaultRetVal;
        } finally {
            trace("Exit getLatestRevision with dDocName: " + dDocName);
        }
    }

    /**
     * Retrieves a DataBinder object configured and built for the specified dID.
     *
     * @param dId the document id to build the DataBinder object for.
     * @return {@link DataBinder} the built DataBinder object.
     */
    public static DataBinder getDocInfoDataBinder(final String dId) throws DataException {
        trace("Enter getDocInfoDataBinder with dId: " + dId);

        try {
            // DOC_INFO_SIMPLE is also much less expensive to call
            final DataBinder serviceBinder = getNewDataBinder();
            serviceBinder.putLocal(AfterCheckinICSStringConstants.IDC_SERVICE, AfterCheckinICSStringConstants.DOC_INFO_SIMPLE);
            serviceBinder.putLocal(AfterCheckinICSStringConstants.KEY_DOC_ID, dId);

            try {
                executeService(serviceBinder);
            } catch (final ServiceException e) {
                warn("Failed to retrieve Content Info for dID: " + dId, e);
            }
            final ResultSet docInfoSet = serviceBinder.getResultSet(AfterCheckinICSStringConstants.DOC_INFO_RS);
            if (docInfoSet != null && !docInfoSet.isEmpty()) {
                setFirstRow(docInfoSet, serviceBinder);
                docInfoSet.closeInternals();
            }
            return serviceBinder;
        } finally {
            trace("Exit getDocInfoDataBinder with dId: " + dId);
        }
    }

    @SuppressWarnings("unchecked")
    private static void setFirstRow(final ResultSet rs, final DataBinder binder) {

        final DataResultSet drs = new DataResultSet();
        drs.copy(rs);
        drs.first();

        final Map<String, String> row = drs.getCurrentRowMap();

        for (final Map.Entry<String, String> field : row.entrySet()) {
            binder.putLocal(field.getKey(), field.getValue());
        }
    }

    /*
    This can sometimes return null. This seems to only occur at server start.
     */
    private static UserData getUserData(final String username) {
        trace("Enter getUserData with username: " + username);

        try {
            return UserStorage.getUserData(username);
        } finally {
            trace("Exit getUserData with username: " + username);
        }
    }

    private static String getUserDataProperty(final String username) {
        trace("Enter getUserDataProperty with username: " + username);

        try {

            String propertyValue = null;

            final UserData data = getUserData(username);
            if (data != null) {
                propertyValue = data.getProperty(AfterCheckinICSStringConstants.D_EMAIL);
            }
            return propertyValue;
        } finally {
            trace("Exit getUserDataProperty with username: " + username);
        }
    }

    public static String getUserEmail(final String username) {
        return getUserDataProperty(username);
    }

    /**
     * Execute a WebCenter Content service.
     * This calls {@link #afterServiceWork(Service)} in a finally block before it exits.
     *
     * @param binder
     * @throws DataException
     * @throws ServiceException
     */
    private static void executeService(final DataBinder binder) throws DataException, ServiceException {
        trace("Start executeService");

        final Service service = getService(binder);
        try {
            service.doUnsecuredRequestInternal();
        } finally {
            WccUtils.afterServiceWork(service);
            trace("Exit executeService");
        }
    }

    private static Workspace getSystemWorkspace() {
        trace("Enter getSystemWorkspace");

        try {
            Workspace result = null;
            final Provider wsProvider = Providers.getProvider("SystemDatabase");
            if (wsProvider != null) {
                result = (Workspace) wsProvider.getProvider();
            }
            return result;
        } finally {
            trace("Exit getSystemWorkspace");
        }
    }

    /**
     * Get a Service. Once our work (such as executing it) with this service is completed, we need to call {@link #afterServiceWork(Service)}
     *
     * @param binder
     * @throws DataException
     * @throws ServiceException
     */
    public static Service getService(final DataBinder binder) throws DataException, ServiceException {
        trace("Start getService in AfterCheckinICS");

        final Workspace ws = getSystemWorkspace();
        try {
            final String idcService = binder.getLocal(AfterCheckinICSStringConstants.IDC_SERVICE);
            // Do not release Workspace because it may yet be needed when the service is executed
            // The Workspace will need to be released later or it will cause "ResourceLimitException"
            // The Service should ultimately have #clear() called on i
            return ServiceManager.getInitializedService(idcService, binder, ws);
        } catch (final DataException e) {
            // service var will always be null here so don't call #afterServiceWork just releaseConnection
            ws.releaseConnection();
            throw e;
        } catch (final ServiceException e) {
            // service var will always be null here so don't call #afterServiceWork just releaseConnection
            ws.releaseConnection();
            throw e;
        } finally {
            trace("End getService in AfterCheckinICS");
        }
    }

    /**
     * Close workspace and call service#clear()
     *
     * @param service
     */
    public static void afterServiceWork(final Service service) {

        if (service != null) {
            final Workspace ws = service.getWorkspace();

            if (ws != null) {
                // Create new map to store connection state
                final HashMap<String, Object> connState = new HashMap<String, Object>();

                // retrieve connection state info and store in map
                ws.getConnectionState(connState);

                // determine whether already in a transaction
                Boolean isInTransaction = Boolean.FALSE;
                final Object isInTransactionObj = connState.get("isInTransaction");
                if (isInTransactionObj != null) {
                    isInTransaction = (Boolean) isInTransactionObj;
                }

                trace("Workspace in transaction? " + isInTransaction);

                // if not in transaction, then release connection
                if (!isInTransaction) {
                    trace("Calling releaseConnection on Workspace");
                    ws.releaseConnection();
                }
            }

            service.clear();
        }
    }

    public static String getEnvironmentValueOrEmptyString(final String environmentKey) {
        String value = SharedObjects.getEnvironmentValue(environmentKey);
        if (value == null) {
            value = "Unknown";
        }
        return value;
    }

    /**
     * Updates the meta data specified by the DataBinder object.
     *
     * @param binder the DataBinder object holding the meta data to update.
     * @since 2.8
     */
    public static boolean updateMetaData(final DataBinder binder) {
        boolean updateSuccessful = false;

        final String dDocName = binder.getLocal(AfterCheckinICSStringConstants.KEY_DOC_NAME);
        trace("dDocName: " + dDocName);

        final String dId = binder.getLocal(AfterCheckinICSStringConstants.KEY_DOC_ID);
        trace("dId: " + dId);

        final String dRevLabel = binder.getLocal(AfterCheckinICSStringConstants.KEY_REV_LABEL);
        trace("dRevLabel: " + dRevLabel);

        final String dSecurityGroup = binder.getLocal(AfterCheckinICSStringConstants.KEY_SECURITY_GROUP);
        trace("dSecurityGroup: " + dSecurityGroup);

        final String dDocAccount = binder.getLocal(AfterCheckinICSStringConstants.KEY_DOC_ACCOUNT);
        trace("dDocAccount: " + dDocAccount);

        binder.putLocal(AfterCheckinICSStringConstants.IDC_SERVICE, "UPDATE_DOCINFO");

        try {
            executeService(binder);
            updateSuccessful = true;
        } catch (final ServiceException se) {
            final String msg = "Unable to update metadata for content id: " + dDocName + ", dID: " + dId + ".";
            warn(msg, se);
        } catch (final DataException de) {
            final String msg = "Unable to update metadata for content id: " + dDocName + ", dID: " + dId + ".";
            warn(msg, de);
        }
        return updateSuccessful;
    }

    public static DataBinder getNewDataBinder() {
        return new DataBinder(SharedObjects.getSafeEnvironment());
    }
}
