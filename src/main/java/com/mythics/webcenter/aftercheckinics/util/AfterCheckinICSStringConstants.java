package com.mythics.webcenter.aftercheckinics.util;

/**
 * @author Jonathan Hult
 */
public class AfterCheckinICSStringConstants {
    public static final String COMPONENT_NAME = "AfterCheckinICS";

    public static final String COMPONENT_ENABLED_PREF_PROMPT = COMPONENT_NAME + "_ComponentEnabled";

    public static final String D_EMAIL = "dEmail";
    public static final String IDC_SERVICE = "IdcService";
    public static final String DOC_INFO_RS = "DOC_INFO";
    public static final String DOC_INFO_SIMPLE = "DOC_INFO_SIMPLE";
    public static final String DOC_INFO_LATESTRELEASE = "DOC_INFO_LATESTRELEASE";

    public static final String KEY_CHECKOUT_USER = "dCheckoutUser";

    public static final String KEY_DOC_AUTHOR = "dDocAuthor";

    public static final String KEY_DOC_TYPE = "dDocType";

    public static final String KEY_DOC_TITLE = "dDocTitle";

    public static final String KEY_SECURITY_GROUP = "dSecurityGroup";

    public static final String KEY_DOC_ACCOUNT = "dDocAccount";

    public static final String KEY_DOC_EXTENSION = "dExtension";

    public static final String KEY_ORIGINAL_FILE_NAME = "dOriginalName";

    public static final String KEY_REV_LABEL = "dRevLabel";

    public static final String KEY_WEB_EXTENSION = "dWebExtension";

    public static final String KEY_DOC_NAME = "dDocName";

    public static final String KEY_DOC_ID = "dID";

    public static final String KEY_RELEASE_STATE = "dReleaseState";

    public static final String KEY_USER_ID = "dUser";
}
