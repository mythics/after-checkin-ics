package com.mythics.webcenter.aftercheckinics.filter;

import com.mythics.webcenter.aftercheckinics.util.AfterCheckinICSStringConstants;
import intradoc.common.ExecutionContext;
import intradoc.data.DataBinder;
import intradoc.data.Workspace;
import intradoc.shared.FilterImplementor;
import intradoc.shared.SharedObjects;

import static com.mythics.webcenter.aftercheckinics.util.AfterCheckinICSStringConstants.COMPONENT_ENABLED_PREF_PROMPT;
import static com.mythics.webcenter.aftercheckinics.util.WccUtils.trace;

/**
 * @author Jonathan Hult
 */
public class AfterCheckinICSPostIndexingStepFilter implements FilterImplementor {

    public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) {

        // only proceed if preference prompt is enabled
        if (SharedObjects.getEnvValueAsBoolean(COMPONENT_ENABLED_PREF_PROMPT, false)) {
            trace("Start postIndexingStep filter for AfterCheckinICS component");

            try {
                // for some reason DataBinder is sometimes null so check it first
                if (binder != null) {

                    final String dDocName = binder.getLocal(AfterCheckinICSStringConstants.KEY_DOC_NAME);
                    // TODO: do something
                }
            } finally {
                trace("End postIndexingStep filter for AfterCheckinICS component");
            }
        }
        // always return CONTINUE or throw an Exception
        // throwing an Exception can cause big issues (like an entire service action to fail) so be cautious if you do this
        return CONTINUE;
    }
}

