# AfterCheckinICS

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* License: MIT
* Last Updated: 0.9.0

## Overview
This WebCenter Content hooks into 2 post-checkin filters and calls an Oracle Integration Cloud service.

* Filters:
	- postWebfileCreation
	- postIndexingStep

* Strings
	- wwAfterCheckinICS_ComponentName

* Tracing sections:
	- aftercheckinics

* Preference Prompts:
	- AfterCheckinICS_ComponentEnabled: boolean determining whether component functionality is enabled.

## Compatibility
This component was built upon and tested on the version listed below. This is the only version it is known to work on, but it may work on older/newer versions.

* 12.2.1.3.0-2017-12-01 04:05:37Z-r158565 (Build:7.3.5.186)

## Changelog
* 0.9.0
	- Initial component creation
